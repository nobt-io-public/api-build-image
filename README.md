# Nobt.io API Build Image

This repository contains the source for the build image of the Nobt.io API.

## Features

- alpine linux
- OpenJDK Java 8
- Cloudfoundry CLI
- jq (Commandline JSON-processor)
- Bash

## Versioning

The produced image follows the semver 2.0 specification.  
As soon as the Dockerfile is modified, the version has to be changed accordingly. 

## Credits

[semver](https://github.com/fsaintjacques/semver-tool): Tiny, BASH-only versioning script 