# How to contribute

1. Create a feature branch for your change
2. Increment the version according to semver 2.0
3. Push your changes
4. Open a MR
5. Delete old snapshot builds if they are not needed