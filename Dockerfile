FROM openjdk:8-jdk-alpine

RUN apk add --update openssl bash libstdc++ python3 git && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    rm -r /root/.cache && \
    rm -rf /var/cache/apk/* && \
    pip install --upgrade --user awsebcli

ENV PATH /env/bin:/root/.local/bin:$PATH